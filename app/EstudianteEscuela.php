<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudianteEscuela extends Model
{
    //
    protected $table = "EstudianteEscuela";
    protected $primaryKey = "est_esc_id";
    protected $fillable = array('est_id','esc_id','est_esc_promocion','est_esc_ingreso','est_esc_est_reg');
    public $timestamps = false;
}
