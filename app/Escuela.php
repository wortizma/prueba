<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    //
     protected $table = "Escuela";
    protected $primaryKey = "esc_id";
    protected $fillable = array('fac_id','esc_nombre','esc_siglas','esc_est_reg');
    public $timestamps = false;
}
