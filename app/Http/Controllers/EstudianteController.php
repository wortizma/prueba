<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageReceived;
use App\Estudiante;
use App\User;
use App\EstudianteEscuela;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\EstudianteProceso;
class EstudianteController extends Controller
{


    public function index(){

        try {
        //Access token from the request
        $token = JWTAuth::parseToken();
        //Try authenticating user
        $user = $token->authenticate();
    } catch (TokenExpiredException $e) {
        //Thrown if token has expired
        return $this->unauthorized('Tu sesion ha expirado, por favor realiza el loguin nuevamente.');
    } catch (TokenInvalidException $e) {
        //Thrown if token invalid
        return $this->unauthorized('Tu credenciales son invalidad, por favor vuelve a loguearte.');
    }catch (JWTException $e) {
        //Thrown if token was not found in the request.
        return $this->unauthorized('Por favor, inicia sesion para continuar.');
    }

    $data = EstudianteProceso::distinct()->select('ProcesoActividad.Nombre','EstudianteProceso.Permiso')->join('ProcesoActividad','ProcesoActividad.IdProcesoActividad' ,'=','EstudianteProceso.IdProcesoActividad')->where([['EstudianteProceso.IdEstudiante',$user->id],['EstudianteProceso.Permiso','1']])->get();
    $Resultado = array();
    foreach ($data as $dat) {

         if (strcmp($dat->Nombre, "Operador") === 0) {
            $Operador = User::select('id','Nombre','idDNITipo','DNI','Correo','Telefono','Direccion','password','Operador','Estudiante','Docente')->where([['Operador',"1"]])->get()->toArray();
            $Resultado = array_merge($Resultado, $Operador);

         }
         if (strcmp($dat->Nombre, "Estudiante") === 0) {
            $Estudiante = User::select('id','Nombre','idDNITipo','DNI','Correo','Telefono','Direccion','password','Operador','Estudiante','Docente')->where([['Estudiante',"1"]])->get()->toArray();
            $Resultado = array_merge($Resultado, $Estudiante);

         }

         if (strcmp($dat->Nombre, "Docente") === 0) {
            $Docente = User::select('id','Nombre','idDNITipo','DNI','Correo','Telefono','Direccion','password','Operador','Estudiante','Docente')->where([['Docente',"1"]])->get()->toArray();
            $Resultado = array_merge($Resultado, $Docente);

         }

    }
         //$Operador->union($Estudiante)->get();
        /*$User = User::select('id','Nombre','idDNITipo','DNI','Correo','password','Operador','Estudiante','Docente')->get();
            return response()->json($User,200);*/
        return response()->json(array_unique($Resultado, SORT_REGULAR));
    }

    public function show($id){
        $User = User::select('id','Nombre','idDNITipo','DNI','Correo','Telefono','Direccion','password','Operador','Estudiante','Docente')->find($id);
        return response()->json($User,200);

    }


    public function update(Request $request, $id)
    {

         request()->validate([
            'Nombre' => 'required|min:6|max:40',
            'DNI' => 'required|numeric',
            'Correo' => 'required|email:rfc,dns',
            'Telefono' => 'required|numeric|digits:9',
            'Direccion' => 'required|min:10|max:50'
        ]);

        $User = User::find($id);
        if(($this->consultar("DNI",$request->DNI,$User->id)) ){
            return  response()->json([
            'Campo' => 'DNI',
            'Existe' => 'true'
            ], 200);
        }

        if(($this->consultar("Correo", $request->Correo,$User->id)) ){
            return  response()->json([
            'Campo' => 'Correo',
            'Existe' => 'true'
            ], 200);
        }


        $User->Nombre = $request->Nombre;
        $User->idDNITipo = $request->idDNITipo;
        $User->DNI = $request->DNI;
        $User->Correo = $request->Correo;
        $User->Telefono = $request->Telefono;
        $User->Direccion = $request->Direccion;
        $User->Operador = $request->Operador;
        $User->Estudiante = $request->Estudiante;
        $User->Docente = $request->Docente;

        if($request->input('password')){
             $User->password = password_hash($request->input('password'),PASSWORD_DEFAULT);
        }
        $User->save();
        return $this->show($id);
    }

    public function consultar($comparar, $dato, $id){
       return User::where($comparar, $dato)->where("id","!=",$id)->exists();;
    }

    public function getStudentPerName(Request $request){

        if ($request->input('Nombre')) {
            $campoBusqueda = 'Nombre';
            $datoEnviado = $request->input('Nombre');
        }else if ($request->input('DNI')) {
            $campoBusqueda = 'DNI';
            $datoEnviado = $request->input('DNI');
        }else if ($request->input('Correo')) {
            $campoBusqueda = 'Correo';
            $datoEnviado = $request->input('Correo');
        }else
            return response()->json(['message' => 'Debe Llena el Campo']);

        $User = User::where($campoBusqueda,'like','%'.$datoEnviado.'%')->select('id','Nombre','idDNITipo','DNI','Correo','Telefono','Direccion','password','Operador','Estudiante','Docente')->get();
                return response()->json($User,200);
    }


}
