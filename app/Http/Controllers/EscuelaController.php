<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Escuela;

class EscuelaController extends Controller
{
    //
     public function index()
    {
        $Escuela = Escuela::select('esc_id','esc_nombre','esc_siglas')->where('esc_est_reg','=','A')->get();
        return response()->json($Escuela, 200);

    }
}
