<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
Route::post('/reiniciar-password','PasswordResetRequestController@sendPasswordResetEmail','as','resetearPass');
Route::post('/change-password', 'ChangePasswordController@passwordResetProcess');
Route::get('/documentos', 'TipodocumentoController@index');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::post('/logout', 'AuthController@logout');
    Route::post('/data', 'AuthController@getAuthUser');
    Route::post('/permission', 'AuthController@getPermission');

    /*Route::get('Operador/getAllOperador','OperadorController@index')->name('getAllOperador');
	Route::post('Operador/createOperador','OperadorController@store')->name('createOperador');
	Route::get('Operador/getOperador/{id}','OperadorController@show')->name('getOperador');
	Route::put('Operador/updateOperador/{id}','OperadorController@update')->name('updateOperador');
	Route::delete('Operador/delete/{id}','OperadorController@destroy')->name('deleteOperador');*/

});
//operador
Route::get('Estudiante/listar', ['middleware' => 'auth.permission:estudiante.listar', 'uses' => 'EstudianteController@index', 'as' => 'EstudianteListar']);
Route::get('Estudiante/ver/{id}', ['middleware' => 'auth.permission:estudiante.ver', 'uses' => 'EstudianteController@show', 'as' => 'EstudianteVer']);
Route::delete('Estudiante/eliminar/{id}', ['middleware' => 'auth.permission:estudiante.eliminar', 'uses' => 'EstudianteController@index', 'as' => 'createOperador']);
Route::get('Estudiante/buscar', ['middleware' => 'auth.permission:estudiante.buscar', 'uses' => 'EstudianteController@getStudentPerName', 'as' => 'EstudianteBuscar']);
Route::put('Estudiante/editar/{id}', ['middleware' => 'auth.permission:estudiante.editar', 'uses' => 'EstudianteController@update', 'as' => 'EstudianteEditar']);

